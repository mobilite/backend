const _ = require('lodash')

module.exports = function rename(arr, operations) {
  let isUnique = false
  if(!Array.isArray(arr)){
    isUnique = true
    arr = [arr]
  }

  let response = _.cloneDeep(arr)

  operations.forEach(operation => {
    response = response.map(item => {

      // allowed interface to work with validate
      operation.from = operation.as || operation.from
      operation.to = operation.name || operation.to

      if(_.get(item, operation.from, null)) {
        // rename field
        if(operation.to) {
          _.set(item, operation.to, _.get(item, operation.from))
          _.unset(item, operation.from)
        }

        // rename value
        if(operation.fromValue && operation.toValue) {
          _.get(item, operation.to || operation.from, null) == operation.fromValue
            ? _.set(item, operation.to || operation.from, operation.toValue)
            : null
        }
      }
      
      return item
    })
  })  

  return isUnique ? response[0] : response
}