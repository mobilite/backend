const app = require('@/app')
const errors = require('@/errors')
const unless = require('express-unless')

module.exports = async (req, res, next) => {

  let authorization = req.headers.authorization

  if(!authorization) throw new errors.BadRequest('Header de autenticação inválido ou não fornecido')

  const Token = app.models.token

  let tokenString = authorization.replace('Bearer', '')

  let token = await Token.findOne({
    active: true,
    token: tokenString
  })

  if(!token) throw new errors.Unauthorized.InvalidToken('Token inválido')

  next()
}

module.exports.unless = unless