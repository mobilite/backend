const app = require('@/app')
const errors = require('@/errors')

module.exports = async(context) => {
  return {
    version: app.config.VERSION,
    date: Date.now(),
    status: 'alive'
  }
}