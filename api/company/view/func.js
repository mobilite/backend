module.exports = async function(){
  return {
    title: 'WeWork Paulista, 1374',
    score: 4,
    recommendations: 27,
    description: 'Ei, ciclista! Venha conhecer o espaço da WeWork Paulista e aproveite para tomar uma ducha. Contamos com os 3 tipos de ducha!',
    telephone: '+55 (11) 4950-5777'
  }
}