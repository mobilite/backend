const app = require('@/app')

module.exports = async(router) => {
  router.get('/company',
    app.helpers.routes.func(require('./func.js')))
}