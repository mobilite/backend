const errors = require('@/errors')

module.exports = async function(context){
  let { type } = context.params

  let types = {
    x: {
      time: '05 min',
      price: '+ R$1,00/min'
    },
    select: {
      time: '10 min',
      price: '+ R$1,00/min',
      aditionals: ['Uma toalha limpa']
    },
    black: {
      time: '15 min',
      price: '+ R$1,00/min',
      aditionals: ['01 toalha limpa', '01 kit banho']
    }
  }

  if(!types[type]) throw new errors.BadRequest(`Invalid type: ${type}`)

  return types[type]
}