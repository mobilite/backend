const app = require('@/app')

module.exports = async(router) => {
  router.get('/optionDetail/:type',
    app.helpers.routes.func(require('./func.js')))
}