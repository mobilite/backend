const app = require('@/app')

module.exports = async(router) => {
  router.post('/user/:userId/paymentMethod',
    app.helpers.routes.func(require('./func.js')))
}