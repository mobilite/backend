const app = require('@/app')

module.exports = async (context) => {

  let { userId } = context.params
  const paymentMethod = app.models.paymentMethod

  if(!userId) throw new errors.BadRequest(`Missing userId`)

  let method = await paymentMethod.find({
    user: userId
  })

  return method
}