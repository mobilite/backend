const app = require('@/app')
const errors = require('@/errors')

const _ = require('lodash')

module.exports = async (context) => {
  const User = app.models.user
  const Token = app.models.token

  let { name, email, password } = context.body

  if(!name || !email || !password) throw new errors.BadRequest(`Missing name, email, password`)

  let user = await User.create({
    name: name,
    email,
    roles: ['cyclist']
  })


  user.setPassword(password)

  await user.save()

  let token = await Token.create({
    active: true,
    token: user.generateJWT()
  })

  return token
}