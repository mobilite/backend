const errors = require('@/errors')
const Axios = require('axios')
const app = require('@/app')

module.exports = async (context) => {
  let { query, lat, lng } = context.query

  if(!query || !lat || !lng) throw new errors.BadRequest(`Missing query, lat, lng`)

  let url = `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${query.replace(' ', '+')}&location=${lat},${lng}&radius=10000&key=${app.config.GOOGLE_API}`
  let data = (await Axios.get(url)).data
  let response = data.results.map(result => {
    return {
      name: result.name,
      rate: result.rating,
      address: result.formatted_address,
      location: {
        lat: result.geometry.location.lat,
        lng: result.geometry.location.lng
      },
      open: result.opening_hours ? result.opening_hours.open_now : false
    }
  })

  return response
}