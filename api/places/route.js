const app = require('@/app')

module.exports = async(router) => {
  router.get('/places',
    app.helpers.routes.func(require('./func.js')))
}