const app = require('@/app')

module.exports = async(router) => {
  router.get('/trip',
    app.helpers.routes.func(require('./func.js')))
}