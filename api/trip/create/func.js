const app = require('@/app')
const errors = require('@/errors')

module.exports = async (context) => {
  const Trip = app.models.trip

  let { user, location } = context.body

  let trip = await Trip.create({
    location
  })

  return await trip.save()
}