const app = require('@/app')

module.exports = async(router) => {
  router.post('/trip',
    app.helpers.routes.func(require('./func.js')))
}