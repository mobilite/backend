# AfterBike API

This project contains the Nodejs interface declaration of the [`AfterBike`]() project.

The project was made during [UberHack](https://www.uber.com/br/pt-br/u/uberhack/), with the aim of improving urban mobility.

## Used technologies

- [Nodejs](https://nodejs.org/en/)
- [Google Cloud](https://cloud.google.com)
- [Heroku](https://www.heroku.com)
- [MongoDB](https://www.mongodb.com/)
- [Mlab](https://mlab.com)
- [Google Maps API](https://developers.google.com/maps/documentation/distance-matrix/intro#travel_modes)
- [Docker](https://www.docker.com/)
- [Iugu](https://iugu.com/)
- [Unit tests with Mocha](https://mochajs.org/)
- [Automatic deploy with GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

## Installation

Just install the AfterBike API with *yarn*:

```
yarn install
```

## Usage

```
docker-compose up
yarn start
```

If you want to run your server continuously, use `yarn start:watch`

## Platform

![AfterBike](https://cdn.edu.tenda.digital/tendaedu/uploads/e4ed08a5-0a67-4860-b3ab-82a1f59e63c1/Landing%20Page.png)

## Company Sign Up

![AfterBike](https://cdn.edu.tenda.digital/tendaedu/uploads/d2e9b518-9b8b-4e2c-83f0-16ac3c840265/Cadastro%20do%20estabelecimento%20-%20Web.png)

### Security

All variables are safe to use only at deploy.
All user password, payment method was fully encrypted.

### Contribuitors

- [Felipe Tiozo](https://gitlab.com/felipet)