require('dotenv').config()

const path = require('path')
const HOUR = (1000 * 60 * 60)
const ip = require('ip')

// Load config variables and expose.
// Load occurs from:
//  > package.json
//  > process.env

module.exports = async(app) => {

  let config = {}

  config.ENV = getEnv('NODE_ENV', 'dev')
  config.PORT = getEnv('PORT') || getEnv('NODE_PORT', 8011)
  config.HOST = getEnv('HOST', `${ip.address()}:${config.PORT}`)
  config.PROTOCOL = getEnv('PROTOCOL', 'http://')
  config.MKT_GENERATOR_API = getEnv('MKT_GENERATOR_API', 'http://localhost:8009/v1')
  config.MONGO_URL = getEnv('MONGO_URL', `mongodb://localhost:27017/mobilite-${config.ENV}`)
  config.SENTRY = getEnv('SENTRY', '')
  config.MASTER_TOKENS = {};
  config.GOOGLE_API = getEnv('GOOGLE_API', 'google_api')
  config.SECRET = getEnv('SECRET', 'secret')

  // state
  config.isProduction = config.ENV == 'production'
  config.isTest = config.ENV == 'test'
  config.isDev = !config.isProduction && !config.isTest

  if(!config.isProduction) {
    config.MASTER_TOKENS['test'] = getEnv('test', '123456')
  }

  // Static assets (dist) configs
  config.distFolder= getEnv('DIST_FOLDER', path.join(__dirname, '../../dist'))
  config.maxAge =  1 * HOUR //

  return config
}

function getEnv(env, defaults) {
  return process.env[env] || defaults
}