const mongoose = require('mongoose')

/*
 * Connects to MongoDB
 */
module.exports = async (app) => {
  // Set custom promisse library to use
  mongoose.Promise = global.Promise

  let driverOptions = { }

  return await mongoose.createConnection(app.config.MONGO_URL, driverOptions)
}
