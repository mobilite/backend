const _ = require('lodash')
const app = require('../app')
const i18n = require('i18n')
const glob = require('glob')
const path = require('path')
const express = require('express')
const fallback = require('express-history-api-fallback')

/*
 * Load routes from api
 */
module.exports = async (app) => {
  // Default route is to show help
  app.server.all('/', (req, res) => {
    res.send({
      hello: 'Fellow developer!',
      contact: 'felipetiozo10@gmail.com',
    })
  })

  // Create secondary router for '/api/*'
  let api = express()

  api.use(i18n.init)

  // If delay mode is enabled, inject middleware to slowdown things
  if (app.config.DEBUG_DELAY) {
    api.use((req, res, next) => {
      setTimeout(next, app.config.DEBUG_DELAY * 1)
    })
  }

  // Install middleware that requires x-community-id
  // api.use(app.helpers.middlewares.community.unless({
  //   path: [
  //     '/v1/status',
  //   ]
  // }))

  // Authenticate user
  // api.use([
  //   app.helpers.middlewares.auth.unless({
  //     path: [
  //       '/v1/status',
  //       { url: '/v1/users', methods: ['POST']  }
  //     ]}),
  //     app.helpers.middlewares.error,
  // ])

  // Locate route files from api folder
  let cwd = path.join(__dirname, '../api')
  let routerPaths = glob.sync('**/*route.js', { cwd })

  // Require route files
  let routers = routerPaths.map(file => require(path.join(cwd, file)))

  // user a temporary router to order
  let tmpRoute = express()

  // Install routes
  for (let route of routers) {
    await route(tmpRoute)
  }

  // Order routes by path priority
  app.helpers.routes.order(tmpRoute)

  // get ordered router and apply on api
  tmpRoute._router.stack.forEach(function (currentRoute){
    let path = _.get(currentRoute, 'route.path')
    let stack = _.get(currentRoute, 'route.stack', [])
    let method = _.get(currentRoute, 'route.stack[0].method')
    let functions = stack.map(s => s.handle)

    if(method) {
      api[method](path, ...functions)
    }
  })

  delete tmpRoute

  // Server errors and Not Found

  api.use('*', app.helpers.middlewares.notFound)
  api.use('*', app.helpers.middlewares.error)

  // Install into `/v1` route
  app.server.use('/v1', api)

  // Default errors
  // app.server.use('*', app.helpers.middlewares.notFound)
  // app.server.use('*', app.helpers.middlewares.error)

  // Fallback
  app.server.use(fallback('index.html', { root: app.config.distFolder }))

  // Return api router
  return api
}
