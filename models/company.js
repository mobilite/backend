// const Contact = require('./common/contact')
const Schema = require('mongoose').Schema
const app = require('@/app')
const _ = require('lodash')

const Model = module.exports = new Schema({
  name: {
    type: String,
    required: true
  },

  kind: {
    type: String,
    enum: ['gym']
  },

  active: {
    type: Boolean,
    default: true
  },

  // contact: Contact,
})