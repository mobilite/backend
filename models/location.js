const Schema = require('mongoose').Schema
const app = require('@/app')
const _ = require('lodash')

const Model = module.exports = new Schema({
  lat: {
    type: Number,
    required: true
  },

  lng: {
    type: Number,
    required: true
  }
})