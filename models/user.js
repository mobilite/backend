const Schema = require('mongoose').Schema
const app = require('@/app')

const _ = require('lodash')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const Model = module.exports = new Schema({

  name: {
    type: String,
    required: true
  },

  email: {
    type: String,
    trim: true,
    unique: true,
    required: 'Email address is required',
    lowercase: true,
  },

  password: {
    type: String
  },

  active: {
    type: Boolean,
    default: true
  },

  roles: {
    type: [String],
    required: true
  }

})

Model.method('setPassword', function (password) {
  this.password = this.constructor.generateHash(password)
})

Model.static('generateHash', (password) => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
})

Model.method('comparePassword', function (password) {
  if ( this.password == null )
    return false

  return bcrypt.compareSync(password, this.password)
})

Model.method('generateJWT', function () {
  const permittedParams = ['name', 'email', 'id']

  let token = _.pick(this, permittedParams)

  // Adds metadata
  token.type = 'auth:user'
  token.date = new Date()

  return jwt.sign(token, app.config.SECRET)
})