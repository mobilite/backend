const Schema = require('mongoose').Schema
const app = require('@/app')
const _ = require('lodash')

const Model = module.exports = new Schema({
  token: {
    type: String,
    required: true
  },

  active: {
    type: Boolean,
    default: true
  }
})