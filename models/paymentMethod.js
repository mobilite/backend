const Schema = require('mongoose').Schema
const app = require('@/app')
const _ = require('lodash')
const errors = require('@/errors')

const Model = module.exports = new Schema({
  name: {
    type: String,
    required: true
  },

  kind: {
    type: String,
    required: true
  },

  last_digits: {
    type: String,
    required: true
  },

  iuguToken: {
    type: String,
    required: true
  }
})