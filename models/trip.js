const Schema = require('mongoose').Schema
const app = require('@/app')
const _ = require('lodash')
const errors = require('@/errors')

const Axios = require('axios')

const Location = require('./location')

const Model = module.exports = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  },

  location: {
    start: {
      type: Location,
      required: true
    },
    end: {
      type: Location,
      required: true
    }
  },

  status: {
    type: String,
    required: true,
    default: 'running',
    enum: ['running', 'finished', 'canceled']
  },

  distance: {
    text: {
      type: String
    },
    value: {
      type: Number
    }
  },

  duration: {
    text: {
      type: String
    },
    value: {
      type: Number
    }
  },

  destination: {
    type: String
  },

  origin: {
    type: String
  }
})

Model.pre('save', async function(){
  if(this.isNew || this.isModified('location')) {
    await this.recomputeTrip()
  }
})

Model.method('recomputeTrip', recomputeTrip)

async function recomputeTrip(){
  let url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${this.location.start.lat},${this.location.start.lng}&destinations=${this.location.end.lat},${this.location.end.lng}&mode=bicycling&sensor=false&language=pt-BR&key=${app.config.GOOGLE_API}`

  try {
    let data = (await Axios.get(url)).data

    if(data.status.includes('REQUEST_DENIED')) throw new Error(JSON.stringify(data))

    this.distance.text = data.rows[0].elements[0].distance.text
    this.distance.value = data.rows[0].elements[0].distance.value
    this.duration.text = data.rows[0].elements[0].duration.text
    this.duration.value = data.rows[0].elements[0].duration.value
    this.destination = data.destination_addresses[0]
    this.origin = data.origin_addresses[0]

  } catch(err) {
    throw new errors.BadRequest(err)
  }
}